package com.akbar.springboottutorialredis.entity;

import jakarta.annotation.Generated;
import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@RedisHash("User")
@Builder
public class User implements Serializable {
    @Id
    private int id;
    private String name;
}
