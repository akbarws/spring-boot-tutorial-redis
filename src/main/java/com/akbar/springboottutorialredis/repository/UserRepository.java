package com.akbar.springboottutorialredis.repository;

import com.akbar.springboottutorialredis.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@AllArgsConstructor
public class UserRepository {

    private static final String HASH_KEY = "User";
    private RedisTemplate<String, Object> redisTemplate;

    public User save(User user) {
        redisTemplate.opsForHash().put(HASH_KEY, user.getId(), user);
        return user;
    }

    public List<User> findAll() {
        return redisTemplate.opsForHash().values(HASH_KEY)
                .stream().map(v -> (User)v)
                .collect(Collectors.toList());
    }

    public User findById(int id) {
        return (User) redisTemplate.opsForHash().get(HASH_KEY, id);
    }

    public int delete(int id) {
        redisTemplate.opsForHash().delete(HASH_KEY, id);
        return id;
    }
}
