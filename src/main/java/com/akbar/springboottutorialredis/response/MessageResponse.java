package com.akbar.springboottutorialredis.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@AllArgsConstructor
public class MessageResponse {
    private HttpStatus code;
    private String message;
    private Object data;
}
