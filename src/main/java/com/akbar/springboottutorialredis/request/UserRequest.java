package com.akbar.springboottutorialredis.request;

import com.fasterxml.jackson.annotation.JsonAlias;
import lombok.Data;

@Data
public class UserRequest {

    private String name;
}
