package com.akbar.springboottutorialredis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootTutorialRedisApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootTutorialRedisApplication.class, args);
    }

}
