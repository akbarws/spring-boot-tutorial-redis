package com.akbar.springboottutorialredis.controller;

import com.akbar.springboottutorialredis.request.UserRequest;
import com.akbar.springboottutorialredis.response.MessageResponse;
import com.akbar.springboottutorialredis.service.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@AllArgsConstructor
public class UserController {

    private UserService userService;

    @PostMapping(value = "/save", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public MessageResponse createUser(@RequestBody UserRequest userRequest) {
        return new MessageResponse(HttpStatus.OK, "Success", userService.createUser(userRequest));
    }

    @GetMapping(value = "/detail/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public MessageResponse getUser(@PathVariable("id") String id) {
        return new MessageResponse(HttpStatus.OK, "Success", userService.getUser(id));
    }

    @GetMapping(value = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public MessageResponse getAllUser() {
        return new MessageResponse(HttpStatus.OK, "Success", userService.getAllUser());
    }
}
