package com.akbar.springboottutorialredis.service;

import com.akbar.springboottutorialredis.entity.User;
import com.akbar.springboottutorialredis.repository.UserRepository;
import com.akbar.springboottutorialredis.request.UserRequest;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
@AllArgsConstructor
public class UserService {

    private UserRepository userRepository;

    public User createUser(UserRequest userRequest) {
        User newUser = User.builder()
                .id(ThreadLocalRandom.current().nextInt(1, 100 + 1))
                .name(userRequest.getName())
                .build();
        return userRepository.save(newUser);
    }

    public User getUser(String id) {
        return userRepository.findById(Integer.parseInt(id));
    }

    public List<User> getAllUser() {
        return userRepository.findAll();
    }
}
